FROM mysql
MAINTAINER Stephen Espy <stephen.espy@stjude.org>
EXPOSE 8080
#
ENV AD=/opt/app
ENV JURL="https://s3.amazonaws.com/bucket-scaa/jdk-8u102-linux-x64.tar.gz"
ENV TURL="https://s3.amazonaws.com/bucket-scaa/apache-tomcat-7.0.70.tar.gz"
ENV JD=jdk1.8.0_102
ENV TD=apache-tomcat-7.0.70
#
RUN apt-get -y update \
	&& apt-get -y install \
		curl \
		vim
RUN mkdir -p ${AD}/bin
#
# provide Java
RUN curl -SL $JURL -o java.tar.gz \
	&& tar -xf java.tar.gz \
	&& rm java.tar.gz \
	&& mv ${JD} ${AD} \
	&& cd ${AD} \
	&& ln -s ${JD} java
#
# provide Tomcat
RUN curl -SL $TURL -o tc.tar.gz \
	&& tar -xf tc.tar.gz \
	&& rm tc.tar.gz \
	&& mv ${TD} ${AD} \
	&& cd ${AD} \
	&& ln -s ${TD} tc
#
# remove unused items in tomcat
RUN cd && cd ${AD}/tc \
	&& rm -rf *E* *U* \
	&& rm -rf ./webapps/*
#
#
CMD ["/bin/bash"]
